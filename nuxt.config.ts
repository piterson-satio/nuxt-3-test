// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app: {
    head: {
      charset: 'utf-16',
      viewport: 'width=500, initial-scale=1',
      title: 'Nuxt 3 Test',
      meta: [
        { name: 'description', content: 'test nuxt 3.' }
      ],
    }
  },
  typescript: {
    typeCheck: true
  }
})
